本仓库为上海交大 ipads 研究所陈海波老师等人所著的《现代操作系统：原理与实现》的课程实验（LAB）的代码。

开源的初衷是我在自学过程中有的知识点理解错了，评测时死活过不了，直到看了前人的实现才找到问题所在。由此感慨自学时无人可以请教，一旦误入牛角尖可能耗费掉大量时间与精力。为了避免后人重蹈覆辙在此将我的代码开源。

课程资源和源码分析请参阅我所写的博客：[ChCore Lab1 机器启动 实验笔记 - 康宇PL - 博客园](https://www.cnblogs.com/kangyupl/p/chcore_lab1.html)

我的所有代码都在 solution 分支下。你如果要从头做可以从 Lab1 分支派生个新分支。如果要查阅某个 Lab 对应的解法可以将 solution 分支进行回退操作。比如查阅 Lab3 的解法那应当回退到与 Lab4 分支合并前的最后一个版本。

# ChCore

This is the repository of ChCore labs in SE315, 2020 Spring.

## build 
  - `make` or `make build`
  - The project will be built in `build` directory.

## Emulate
  - `make qemu`

  Emulate ChCore in QEMU

## Debug with GBD

  - `make qemu-gdb`

  Start a GDB server running ChCore
  
  - `make gdb`
  
  Start a GDB (gdb-multiarch) client

## Grade
  - `make grade`
  
  Show your grade of labs in the current branch

## Other
  - type `Ctrl+a x` to quit QEMU
  - type `Ctrl+d` to quit GDB
  